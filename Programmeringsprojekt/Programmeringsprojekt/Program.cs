﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programmeringsprojekt {

    public class main {
        public static void Main(string[] args) {
            Varehus nytVarehus = new Varehus();
            nytVarehus.Run();
        }
    }

    // Her opretter variablerne der bruges i en vare, og inde i vores Queues.
    class Vare {
        public int udloebsdato;
        public int maengde;
        public string navn;

        public Vare(int maengde, int udloebsdato, string navn) {

            this.maengde = maengde;
            this.udloebsdato = udloebsdato;
            this.navn = navn;

        }
    }

    //Her opretter vi vores dictionary, og inde i den vores Queues som er til vores varer.
    public class Varehus {
        Dictionary<string, Queue<Vare>> vareHusLager = new Dictionary<string, Queue<Vare>>();
        int dag = 1;
        int nyLevering = 7;

        public Varehus() {

            vareHusLager.Add("Koteletter", new Queue<Vare>());
            vareHusLager["Koteletter"].Enqueue(new Vare(10, 10, "Koteletter"));

            vareHusLager.Add("Kylling", new Queue<Vare>());
            vareHusLager["Kylling"].Enqueue(new Vare(10, 7, "Kylling"));

            vareHusLager.Add("Gulerødder", new Queue<Vare>());
            vareHusLager["Gulerødder"].Enqueue(new Vare(10, 12, "Gulerødder"));

            vareHusLager.Add("Agurker", new Queue<Vare>());
            vareHusLager["Agurker"].Enqueue(new Vare(10, 17, "Agurker"));

        }

        //Denne funktion giver brugeren lov til at kunne tage en vare, og fjerne dermed en vare fra Queue'en, den kommer fra.
        void TagVare() {
            Console.WriteLine();
            Console.WriteLine("Hvilken vare ønsker du at tage?");
            NuværendeVarer();
            string checkInput = Console.ReadLine();

            if (vareHusLager.ContainsKey(checkInput)) {

                if (vareHusLager[checkInput].Count != 0) {

                    Vare foersteVare = vareHusLager[checkInput].Peek();

                    if (foersteVare.maengde > 0) {
                        foersteVare.maengde -= 1;

                        Console.WriteLine("Du har nu taget en pakke " + checkInput + " og der befinder sig nu: " + foersteVare.maengde + " pakke(r) tilbage på lageret");
                    }

                    else {
                        Console.WriteLine("Der er ikke flerer varer tilbage af denne slags");
                    }

                }

            }

            else {
                Console.WriteLine("Der er ikke nogen varer af denne slags");
            }

        }

        //Denne funktion holder styr på hvor mange dage der er gået og fylder queues op når der er gået et vidst antal dage.
        //Den tjekker også om der er flere vare i en queue, og ellers ignorer den, den.
        void NyDag() {

                Console.Clear();
                dag += 1;
                nyLevering -= 1;
                Console.WriteLine("Dag " + dag);

            if (vareHusLager.Count != 0) {

                if (vareHusLager["Koteletter"].Count != 0) {
                    Vare koteletterUdloeb = vareHusLager["Koteletter"].Peek();

                    if (koteletterUdloeb.udloebsdato > 0) {
                        koteletterUdloeb.udloebsdato -= 1;
                    }

                    if (koteletterUdloeb.udloebsdato <= 0) {
                        vareHusLager["Koteletter"].Dequeue();
                    }
                }

                if (vareHusLager["Kylling"].Count != 0) {
                    Vare kyllingUdloeb = vareHusLager["Kylling"].Peek();

                    if (kyllingUdloeb.udloebsdato > 0) {
                        kyllingUdloeb.udloebsdato -= 1;
                    }

                    else if (kyllingUdloeb.udloebsdato <= 0) {
                        vareHusLager["Kylling"].Dequeue();
                    }
                }

                if (vareHusLager["Kylling"].Count != 0) {
                    Vare guleroedderUdloeb = vareHusLager["Gulerødder"].Peek();

                    if (guleroedderUdloeb.udloebsdato > 0) {
                        guleroedderUdloeb.udloebsdato -= 1;
                    }

                    else if (guleroedderUdloeb.udloebsdato <= 0) {
                        vareHusLager["Gulerødder"].Dequeue();
                    }
                }

                if (vareHusLager["Kylling"].Count != 0) {
                    Vare agurkerUdloeb = vareHusLager["Agurker"].Peek();

                    if (agurkerUdloeb.udloebsdato > 0) {
                        agurkerUdloeb.udloebsdato -= 1;
                    }

                    else if (agurkerUdloeb.udloebsdato <= 0) {
                        vareHusLager["Agurker"].Dequeue();
                    }
                }

            }

            if (nyLevering <= 0) {
                NyLevering();
            }

        }

        //Denne funktion fylder queues i Dictionary'et op igen.
        void NyLevering() {
            nyLevering = 7;
            vareHusLager["Koteletter"].Clear();
            vareHusLager["Koteletter"].Enqueue(new Vare(10, 10, "Koteletter"));
            vareHusLager["Kylling"].Clear();
            vareHusLager["Kylling"].Enqueue(new Vare(10, 7, "Kylling"));
            vareHusLager["Gulerødder"].Clear();
            vareHusLager["Gulerødder"].Enqueue(new Vare(10, 12, "Gulerødder"));
            vareHusLager["Agurker"].Clear();
            vareHusLager["Agurker"].Enqueue(new Vare(10, 17, "Agurker"));
        }

        //Denne funktion udskriver alle de vare der er blevet oprettet i vores Dictionary.
        void NuværendeVarer() {
            Dictionary<string, Queue<Vare>>.KeyCollection keyColl = vareHusLager.Keys;
            Console.WriteLine("Du kan vælge mellem:");
            // Funktionen nedenunder er delvist taget fra: https://msdn.microsoft.com/en-us/library/yt2fy5zk(v=vs.110).aspx
            foreach (string s in keyColl) {
                Console.Write("{0} ", s);
            }

            Console.WriteLine();

        }

        //Denne funktion kører koden i konsollen og giver brugeren muligheden for at give inputs.
        public void Run() {

            Console.WriteLine("Dag " + dag);
            if (nyLevering == 7) {
                Console.WriteLine("Der er ankommet nye varer");
            }

            while (true) {
                //Console.Clear();

                Console.WriteLine();
                Console.WriteLine("Hvad vil du gerne foretage dig?");
                Console.WriteLine();
                Console.WriteLine("Skriv 'Ny dag' for at starte en ny dag");
                Console.WriteLine("Skriv 'Tag vare' for at tage en vare ud af systemet");
                Console.WriteLine("Skriv 'Exit' for at lukke programmet ned");
                string input = Console.ReadLine();
                string checkInput = input.ToUpper();

                if (checkInput == "NY DAG") {
                    NyDag();

                }

                else if (checkInput == "TAG VARE") {
                    TagVare();
                }


                else if (checkInput == "EXIT") {
                    break;
                }

                else {
                    Console.WriteLine("Det er der ikke noget der hedder");
                }

            }

        }
            
    }
    
}

   

